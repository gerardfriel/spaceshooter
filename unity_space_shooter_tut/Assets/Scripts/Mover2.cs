﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover2 : MonoBehaviour {

    //Unity >5 use rb ref
    private Rigidbody rb;

    //Setup speed variable editable in the editor
    public float speed;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        rb.velocity = transform.forward * speed;

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
